include("../src/ITCPGanalysis.jl");

using .ITCPGanalysis
using FileIO

function save_critical_lines_static(; output_file="")
    P = PaperParameters() # load parameters used in the paper
    if isempty(output_file)
        output_file = joinpath(
            @__DIR__,
            "data/Critical_lines_static_rbrk$(P.collapse_rate_h).jld2"
        )
    end

    if isfile(output_file)
        println("File $output_file already exists.")
        return nothing
    end

    critical_lines = calc_conditional_large_cascade_probs(P.hurricanes)
    # Exclude nan or inf values and sort lines
    filter!(entry -> (!isnan(entry[2]) && !isinf(entry[2])), critical_lines)
    critical_lines_sorted = sort(critical_lines, byvalue=true, rev=true)

    save(output_file, "critical_lines", critical_lines_sorted)

    println(
        "Critical lines saved successfully in $output_file."
    )

    return nothing
end

save_critical_lines_static()

critical_lines = load(
    joinpath(@__DIR__, "data/Critical_lines_static_rbrk0.002.jld2"),
    "critical_lines"
)
