include("../src/ITCPGanalysis.jl");

using .ITCPGanalysis
using FileIO

function save_cascade_count_data(; output_file="")
    P = PaperParameters() # load parameters used in the paper
    if isempty(output_file)
        output_file = joinpath(
            @__DIR__,
            "data/Cascade_count_data_rbrk$(P.collapse_rate_h).jld2"
        )
    end

    if isfile(output_file)
        println("File $(output_file) already exists.")
        return nothing
    end

    cascade_count_data = calc_cascade_count_data(
        P.hurricanes,
        collapse_rate_h = P.collapse_rate_h,
        threshold_gw = P.threshold_gw
    )

    save(output_file, "cascade_count_data", cascade_count_data)

    println(
        "Cascade count data saved successfully in $(output_file)."
    )

    return nothing
end

function save_critical_lines_coevolution(; input_file="", output_file="")
    P = PaperParameters() # load parameters used in the paper
    if isempty(input_file)
        input_file = joinpath(
            @__DIR__,
            "data/Cascade_count_data_rbrk$(P.collapse_rate_h).jld2"
        )
    end
    if isempty(output_file)
        output_file = joinpath(
            @__DIR__,
            "data/Critical_lines_coevolution_rbrk$(P.collapse_rate_h).jld2"
        )
    end

    if isfile(output_file)
        println("File $output_file already exists.")
        return nothing
    end

    critical_lines = calc_priority_indices(input_file=input_file)
    critical_lines_sorted = sort(critical_lines, byvalue=true, rev=true)

    save(output_file, "critical_lines", critical_lines_sorted)

    println(
        "Critical lines saved successfully in $output_file."
    )

    return nothing
end

save_cascade_count_data()

save_priority_indices()

critical_lines = load(
    joinpath(@__DIR__, "data/Critical_lines_coevolution_rbrk0.002.jld2"),
    "critical_lines"
)
