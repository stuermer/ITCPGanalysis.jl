module ITCPGanalysis

using FileIO
using StatsBase
using Parameters

@with_kw struct PaperParameters
    hurricanes::Vector{String} = [
        "Harvey",
        "Ike",
        "Claudette",
        "Hanna",
        "Erin",
        "Hermine",
        "Laura"
    ]
    collapse_rate_h::Float64 = 0.002
    threshold_gw::Float64 = 15.0
end
export PaperParameters

include("network_data.jl")
export
    is_underground,
    get_bus_loads,
    get_underground_lines

include("critical_lines.jl")
export
    calc_cascade_count_data,
    calc_priority_indices,
    calc_conditional_large_cascade_probs

end
