function calc_cascade_count_data(
    hurricanes::Vector{String};
    collapse_rate_h = 0.002,
    threshold_gw    = 15.0
)
    data = Dict(
        name => Dict{String,Any}(
            "number_cascades" => 0,
            "number_large_cascades" => 0,
            "large_cascade_countmap" => Dict{Vector{String},Int64}()
        ) for name in hurricanes
    )

    for name in hurricanes
        data_dir = joinpath(
            @__DIR__,
            "../../simulation_data/hurricane_$(lowercase(name))/",
            "$(lowercase(name))_results_dc_res5min_rbrk$collapse_rate_h/"
        )
        large_cascade_events = Vector{Vector{String}}()
        for file in readdir(data_dir)
            result = load(data_dir * file, "result")
            for scenario in values(result)
                for (f, cascade) in scenario["history"]
                    if !any(["overload" ∈ c for c in values(cascade)])
                        continue
                    end

                    supplied_load_start = cascade[1][1]
                    supplied_load_end = maximum(cascade)[2][1]
                    pu_to_gw = 0.1
                    cascade_size_gw = (
                        (supplied_load_start - supplied_load_end) * pu_to_gw
                    )

                    if cascade_size_gw > 0.0
                        data[name]["number_cascades"] += 1
                    end

                    if cascade_size_gw >= threshold_gw
                        data[name]["number_large_cascades"] += 1
                        destroyed_lines = [
                            d[1] for d in scenario["primary_dmg"] if d[2] == f
                        ]
                        push!(large_cascade_events, destroyed_lines)
                    end
                end
            end
        end

        if isempty(large_cascade_events)
            println("No critical events found for $(hurricane).")
            continue
        end

        data[name]["large_cascade_countmap"] = countmap(large_cascade_events)
    end

    return data
end

function calc_priority_indices(; input_file="", number_sim=1e4, kwargs...)
    network_data = load(
        joinpath(@__DIR__, "../../input_data/Texas_init_dcpf.jld2"),
        "network_data"
    )
    priority_indices = Dict(
        id => 0.0 for (id, br) in network_data["branch"]
        if br["transformer"] == false
        && br["source_id"][end] == "1 "
        && !is_underground(network_data, id)
    )

    count_data = Dict{String,Any}()
    if !isempty(input_file)
        count_data = load(input_file, "cascade_count_data")
    else
        count_data = calc_cascade_count_data(hurricanes; kwargs...)
    end
    hurricanes = values(count_data)
    number_hurricanes = length(hurricanes)

    for line in keys(priority_indices)
        weighted_sum = 0.0
        for name in hurricanes
            if !haskey(name["large_cascade_countmap"], [line])
                continue
            end
            weighted_sum += name["large_cascade_countmap"][[line]] / number_sim
        end
        priority_indices[line] = weighted_sum / number_hurricanes
    end

    return priority_indices
end

function calc_conditional_large_cascade_probs(
    hurricanes::Vector{String};
    kwargs...
)
    network_data = load(
        joinpath(@__DIR__, "../../input_data/Texas_init_dcpf.jld2"),
        "network_data"
    )
    destructible_lines = [
        id for (id, br) in network_data["branch"]
        if br["transformer"] == false
        && br["source_id"][end] == "1 "
        && !is_underground(network_data, id)
    ]
    count_data = Dict(
        name => Dict(
            id => Dict(
                "number_large_cascades" => 0,
                "number_destruction_events" => 0
            ) for id in destructible_lines
        ) for name in hurricanes
    )

    for name in hurricanes
        _populate_static_cascade_count_data!(count_data[name], name; kwargs...)
    end

    result = Dict(
        id => mean([
            data[id]["number_large_cascades"] / data[id]["number_destruction_events"]
            for data in values(count_data)
        ]) for id in destructible_lines
    )

    return result
end

function _populate_static_cascade_count_data!(
    count_data::Dict{String,Dict{String,Int64}},
    hurricane::String;
    collapse_rate_h = 0.002,
    threshold_gw = 15.0
)
    init_load_mw = 671.0923499999997
    datadir = joinpath(
        @__DIR__,
        "../../simulation_data/hurricane_$(lowercase(hurricane))/",
        "$(lowercase(hurricane))_results_dc_res5min_rbrk$(collapse_rate_h)/"
    )
    for file in readdir(datadir)
        simulation_data = load(datadir * file, "ntr_result")
        for scenario in values(simulation_data)
            power_outage_gw = (init_load_mw - scenario["final_MW_load"]) * 0.1
            destroyed_lines = [p[1] for p in scenario["primary_dmg"]]
            for line in destroyed_lines
                count_data[line]["number_destruction_events"] += 1
                if power_outage_gw >= threshold_gw
                    count_data[line]["number_large_cascades"] += 1
                end
            end
        end
    end

    return nothing
end