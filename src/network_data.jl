function is_underground(
    network_data::Dict{String,<:Any},
    id::String;
    max_length_km      = 12.875,
    min_connected_load = 2.0, # in per-unit (100MW base power)
    consider_both_ends = true
)
    branch = network_data["branch"][id]
    if branch["transformer"] == true || branch["length"] >= max_length_km
        return false
    end

    connected_load = _calc_connected_load(
        network_data,
        branch,
        consider_both_ends
    )

    if connected_load >= min_connected_load
        return true
    else
        return false
    end
end

function _calc_connected_load(
    network_data::Dict{String,<:Any},
    branch::Dict{String,<:Any},
    consider_both_ends = true
)
    loads_from = [
        l["pd"] for l in values(network_data["load"])
        if l["load_bus"] == branch["f_bus"] && l["status"] == 1
    ]
    connected_load_from = sum(loads_from, init=0.0)
    loads_to = [
        l["pd"] for l in values(network_data["load"])
        if l["load_bus"] == branch["t_bus"] && l["status"] == 1
    ]
    connected_load_to = sum(loads_to, init=0.0)

    connected_load = 0.0
    if consider_both_ends
        connected_load = connected_load_from + connected_load_to
    else
        connected_load = max(connected_load_from, connected_load_to)
    end

    return connected_load
end

function get_bus_loads(network_data::Dict{String,<:Any})
    bus_loads = Dict{String,Float64}()

    for load in values(network_data["load"])
        load_dict = Dict("$(load["load_bus"])" => load["pd"])
        merge!(+, bus_loads, load_dict)
    end

    return bus_loads
end

function get_underground_lines(network_data::Dict{String,<:Any}; kwargs...)
    underground_lines = [
        id for id in keys(network_data["branch"])
        if is_underground(network_data, id; kwargs...)
    ]
    return underground_lines
end
