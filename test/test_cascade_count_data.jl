@testset "Cascade count data" begin
    result = calc_cascade_count_data(["Claudette"])
    count_data = load(
        "data/Claudette_cascade_count_data_rbrk0.002.jld2",
        "count_data"
    )

    @test result["Claudette"] == count_data
end