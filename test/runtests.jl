using ITCPGanalysis
using Test
using FileIO

for name in (
    "underground_lines",
    "cascade_count_data"
)
    @testset "test_$name" begin
        include("test_$name.jl")
    end
end