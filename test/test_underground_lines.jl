@testset "Underground lines" begin
    network_data = load("data/Texas_init_dcpf.jld2", "network_data")
    result = get_underground_lines(network_data)

    underground_lines = load(
        "data/Texas_underground_lines.jld2",
        "underground_lines"
    )

    @test length(result) == length(underground_lines)
    @test !any(result .∉ Ref(underground_lines))
end